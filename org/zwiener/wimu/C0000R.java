package org.zwiener.wimu;

public final class C0000R {

    public static final class array {
        public static final int delay_array = 2131034112;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
    }

    public static final class id {
        public static final int checkBackground = 2131099665;
        public static final int editAddress = 2131099651;
        public static final int editPort = 2131099653;
        public static final int spinnerDelay = 2131099655;
        public static final int tableRow2 = 2131099656;
        public static final int tableRow3 = 2131099659;
        public static final int tableRow4 = 2131099662;
        public static final int textAcc = 2131099658;
        public static final int textAddress = 2131099650;
        public static final int textCopyright = 2131099666;
        public static final int textGyro = 2131099661;
        public static final int textLabelAcc = 2131099657;
        public static final int textLabelGyro = 2131099660;
        public static final int textLabelMag = 2131099663;
        public static final int textLabelRate = 2131099654;
        public static final int textMag = 2131099664;
        public static final int textPort = 2131099652;
        public static final int textSend = 2131099648;
        public static final int toggleSensor = 2131099649;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int delay_prompt = 2130968595;
        public static final int error_invalidaddr = 2130968590;
        public static final int error_neterror = 2130968591;
        public static final int error_sensorerror = 2130968592;
        public static final int error_warningwifi = 2130968593;
        public static final int label_acc = 2130968577;
        public static final int label_broadcast = 2130968581;
        public static final int label_copyright = 2130968594;
        public static final int label_defaultaddr = 2130968587;
        public static final int label_defaultport = 2130968588;
        public static final int label_gyro = 2130968578;
        public static final int label_ipaddr = 2130968582;
        public static final int label_ipport = 2130968583;
        public static final int label_mag = 2130968579;
        public static final int label_runinbackground = 2130968589;
        public static final int label_sendactive = 2130968585;
        public static final int label_sendinactive = 2130968586;
        public static final int label_updaterate = 2130968584;
        public static final int label_zerosensor = 2130968580;
    }
}
