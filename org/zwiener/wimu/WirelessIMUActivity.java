package org.zwiener.wimu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Locale;

public class WirelessIMUActivity extends Activity implements OnClickListener, SensorEventListener, OnItemSelectedListener {
    private static final int CSV_ID_ACCELEROMETER = 3;
    private static final int CSV_ID_GYROSCOPE = 4;
    private static final int CSV_ID_MAG = 5;
    private static final double NS2S = 9.999999717180685E-10d;
    private static final double mMaxSecDiff = 0.5d;
    private double[] mAccBuffer = new double[CSV_ID_ACCELEROMETER];
    private boolean mAccBufferReady = false;
    private double mAccTime = 0.0d;
    private boolean mActive = false;
    private CheckBox mCheckBoxBackground;
    private int mCounter;
    private int mDelay = CSV_ID_ACCELEROMETER;
    private EditText mEditAddr;
    private EditText mEditPort;
    private double[] mGyroBuffer = new double[CSV_ID_ACCELEROMETER];
    private boolean mGyroBufferReady = false;
    private double mGyroTime = 0.0d;
    private double[] mMagBuffer = new double[CSV_ID_ACCELEROMETER];
    private boolean mMagBufferReady = false;
    private double mMagTime = 0.0d;
    private DatagramPacket mPacket = null;
    private SensorManager mSenMan;
    private DatagramSocket mSocket = null;
    private Spinner mSpinnerDelay;
    private TextView mTextAcc;
    private TextView mTextGyro;
    private TextView mTextMag;
    private ToggleButton mToggleButtonSensor;

    class C00011 implements DialogInterface.OnClickListener {
        C00011() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0000R.layout.main);
        if (VERSION.SDK_INT > 9) {
            StrictMode.setThreadPolicy(new Builder().permitAll().build());
        }
        this.mSenMan = (SensorManager) getSystemService("sensor");
        this.mTextAcc = (TextView) findViewById(C0000R.id.textAcc);
        this.mTextGyro = (TextView) findViewById(C0000R.id.textGyro);
        this.mTextMag = (TextView) findViewById(C0000R.id.textMag);
        this.mEditAddr = (EditText) findViewById(C0000R.id.editAddress);
        this.mEditPort = (EditText) findViewById(C0000R.id.editPort);
        this.mCheckBoxBackground = (CheckBox) findViewById(C0000R.id.checkBackground);
        this.mToggleButtonSensor = (ToggleButton) findViewById(C0000R.id.toggleSensor);
        this.mToggleButtonSensor.setOnClickListener(this);
        this.mSpinnerDelay = (Spinner) findViewById(C0000R.id.spinnerDelay);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, C0000R.array.delay_array, 17367048);
        adapter.setDropDownViewResource(17367049);
        this.mSpinnerDelay.setAdapter(adapter);
        this.mSpinnerDelay.setOnItemSelectedListener(this);
        this.mSpinnerDelay.setSelection(CSV_ID_ACCELEROMETER);
    }

    protected void onResume() {
        super.onResume();
        if (this.mSocket != null) {
            this.mToggleButtonSensor.setChecked(true);
        }
    }

    protected void onPause() {
        super.onPause();
        if (!this.mCheckBoxBackground.isChecked()) {
            this.mToggleButtonSensor.setChecked(false);
            stopWIMU();
        }
    }

    private boolean startSensor() {
        this.mCounter = 1;
        try {
            this.mSenMan.registerListener(this, this.mSenMan.getDefaultSensor(1), this.mDelay);
            int validSensors = 0 + 1;
            try {
                this.mSenMan.registerListener(this, this.mSenMan.getDefaultSensor(CSV_ID_GYROSCOPE), this.mDelay);
                validSensors++;
            } catch (Exception e) {
            }
            try {
                this.mSenMan.registerListener(this, this.mSenMan.getDefaultSensor(2), CSV_ID_ACCELEROMETER);
                validSensors++;
            } catch (Exception e2) {
            }
            if (validSensors >= 1) {
                return true;
            }
            return false;
        } catch (Exception e3) {
            return false;
        }
    }

    private void stopSensor() {
        this.mSenMan.unregisterListener(this);
        this.mAccBufferReady = false;
        this.mGyroBufferReady = false;
        this.mMagBufferReady = false;
    }

    private void problemDialog(int stringid) {
        AlertDialog ad = new AlertDialog.Builder(this).create();
        ad.setCancelable(false);
        ad.setMessage(getString(stringid));
        ad.setButton("OK", new C00011());
        ad.show();
    }

    private boolean isOnWifi() {
        return ((ConnectivityManager) getSystemService("connectivity")).getNetworkInfo(1).isConnectedOrConnecting();
    }

    private boolean startUDP() {
        if (!isOnWifi()) {
            problemDialog(C0000R.string.error_warningwifi);
        }
        try {
            InetAddress addr = InetAddress.getByName(this.mEditAddr.getText().toString());
            try {
                this.mSocket = new DatagramSocket();
                this.mSocket.setReuseAddress(true);
                byte[] buf = new byte[128];
                try {
                    this.mPacket = new DatagramPacket(buf, buf.length, addr, Integer.parseInt(this.mEditPort.getText().toString()));
                    return true;
                } catch (Exception e) {
                    this.mSocket.close();
                    this.mSocket = null;
                    problemDialog(C0000R.string.error_neterror);
                    return false;
                }
            } catch (SocketException e2) {
                this.mSocket = null;
                problemDialog(C0000R.string.error_neterror);
                return false;
            }
        } catch (UnknownHostException e3) {
            problemDialog(C0000R.string.error_invalidaddr);
            return false;
        }
    }

    private void stopUDP() {
        if (this.mSocket != null) {
            this.mSocket.close();
        }
        this.mSocket = null;
        this.mPacket = null;
    }

    private boolean startWIMU() {
        if (!startUDP()) {
            return false;
        }
        if (startSensor()) {
            this.mEditAddr.setEnabled(false);
            this.mEditPort.setEnabled(false);
            this.mSpinnerDelay.setEnabled(false);
            this.mActive = true;
            return true;
        }
        stopUDP();
        problemDialog(C0000R.string.error_sensorerror);
        return false;
    }

    private void stopWIMU() {
        stopSensor();
        stopUDP();
        this.mEditAddr.setEnabled(true);
        this.mEditPort.setEnabled(true);
        this.mSpinnerDelay.setEnabled(true);
        this.mActive = false;
    }

    protected void onDestroy() {
        super.onDestroy();
        stopWIMU();
    }

    public void onClick(View arg0) {
        if (arg0.getId() == C0000R.id.toggleSensor) {
            if (this.mActive) {
                stopWIMU();
            } else {
                startWIMU();
            }
            this.mToggleButtonSensor.setChecked(this.mActive);
        }
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    public void onSensorChanged(SensorEvent event) {
        double timestamp_sec = ((double) event.timestamp) * NS2S;
        double x = (double) event.values[0];
        double y = (double) event.values[1];
        double z = (double) event.values[2];
        switch (event.sensor.getType()) {
            case 1:
                this.mCounter++;
                this.mAccBuffer[0] = x;
                this.mAccBuffer[1] = y;
                this.mAccBuffer[2] = z;
                this.mAccBufferReady = true;
                this.mAccTime = timestamp_sec;
                break;
            case 2:
                this.mMagBuffer[0] = x;
                this.mMagBuffer[1] = y;
                this.mMagBuffer[2] = z;
                this.mMagBufferReady = true;
                this.mMagTime = timestamp_sec;
                break;
            case CSV_ID_GYROSCOPE /*4*/:
                this.mGyroBuffer[0] = x;
                this.mGyroBuffer[1] = y;
                this.mGyroBuffer[2] = z;
                this.mGyroBufferReady = true;
                this.mGyroTime = timestamp_sec;
                break;
            default:
                return;
        }
        if (this.mCounter % 15 == 1) {
            Object[] objArr = new Object[CSV_ID_ACCELEROMETER];
            objArr[0] = Double.valueOf(x);
            objArr[1] = Double.valueOf(y);
            objArr[2] = Double.valueOf(z);
            String stext = String.format("%6.3f %6.3f %6.3f", objArr);
            switch (event.sensor.getType()) {
                case 1:
                    this.mTextAcc.setText(stext);
                    break;
                case 2:
                    this.mTextMag.setText(stext);
                    break;
                case CSV_ID_GYROSCOPE /*4*/:
                    this.mTextGyro.setText(stext);
                    break;
            }
        }
        if (this.mAccBufferReady) {
            boolean gyroReady = Math.abs(this.mGyroTime - this.mAccTime) < mMaxSecDiff && this.mGyroBufferReady;
            boolean magReady = Math.abs(this.mMagTime - this.mAccTime) < mMaxSecDiff && this.mMagBufferReady;
            StringBuilder strBuilder = new StringBuilder(128);
            strBuilder.append(String.format(Locale.ENGLISH, "%.5f", new Object[]{Double.valueOf(timestamp_sec)}));
            addSensorToString(strBuilder, CSV_ID_ACCELEROMETER, this.mAccBuffer[0], this.mAccBuffer[1], this.mAccBuffer[2]);
            this.mAccBufferReady = false;
            if (gyroReady) {
                addSensorToString(strBuilder, CSV_ID_GYROSCOPE, this.mGyroBuffer[0], this.mGyroBuffer[1], this.mGyroBuffer[2]);
                this.mGyroBufferReady = false;
            }
            if (magReady) {
                addSensorToString(strBuilder, CSV_ID_MAG, this.mMagBuffer[0], this.mMagBuffer[1], this.mMagBuffer[2]);
                this.mMagBufferReady = false;
            }
            sendTo(strBuilder.toString());
        }
    }

    private static void addSensorToString(StringBuilder strBuilder, int sensorid, double x, double y, double z) {
        Object[] objArr = new Object[CSV_ID_GYROSCOPE];
        objArr[0] = Integer.valueOf(sensorid);
        objArr[1] = Double.valueOf(x);
        objArr[2] = Double.valueOf(y);
        objArr[CSV_ID_ACCELEROMETER] = Double.valueOf(z);
        strBuilder.append(String.format(Locale.ENGLISH, ", %d, %7.3f,%7.3f,%7.3f", objArr));
    }

    private void sendTo(String s) {
        try {
            byte[] bytes = s.getBytes("UTF-8");
            if (this.mPacket != null && this.mSocket != null) {
                this.mPacket.setData(bytes);
                this.mPacket.setLength(bytes.length);
                try {
                    this.mSocket.send(this.mPacket);
                } catch (Exception e) {
                }
            }
        } catch (UnsupportedEncodingException e2) {
        }
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
        if (pos == 0) {
            this.mDelay = 0;
        } else if (pos == 1) {
            this.mDelay = 1;
        } else if (pos == 2) {
            this.mDelay = 2;
        } else if (pos == CSV_ID_ACCELEROMETER) {
            this.mDelay = CSV_ID_ACCELEROMETER;
        } else {
            this.mDelay = CSV_ID_ACCELEROMETER;
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
